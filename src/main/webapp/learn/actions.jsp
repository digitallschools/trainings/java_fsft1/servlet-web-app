<%-- 
    Document   : actions
    Created on : 03-Jan-2020, 2:52:39 PM
    Author     : DigitallSchool <rupeshkumar@digitallschool.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.digitallschool.training.spiders.web.Car" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>

        <%
            Car t = new Car();
            out.println(t.getBrand());
        %>

        <br/>

        <jsp:useBean id="car1" scope="page" class="Car"/>

        BRAND: <jsp:getProperty name="car1" property="brand"/>
        <br/>
        <jsp:setProperty name="car1" property="brand" value="Audi"/>
        BRAND: <jsp:getProperty name="car1" property="brand"/>

        <br/>
        <jsp:setProperty name="car1" property="color" param="col"/>
        COLOR: <jsp:getProperty name="car1" property="color"/>

        <br/>

        <jsp:useBean id="car2" scope="page" class="Car"/>
        <jsp:setProperty name="car2" property="*"/>
        BRAND: <jsp:getProperty name="car2" property="brand"/><br/>
        Model: <jsp:getProperty name="car2" property="model"/><br/>
        COLOR: <jsp:getProperty name="car2" property="color"/><br/>
        Price: <jsp:getProperty name="car2" property="price"/><br/>
        <br/><br/>
        
        <%
            if (request.getParameter("name") == null || request.getParameter("name") == "") {
        %>
        Please give your name
        <%
        } else {
        %>
            Name: <%= request.getParameter("name")%>
        <%
            }
        %>
        <br/><br/>

        <%--
        <jsp:plugin code="" codebase="" type="">
            
            <jsp:params>
                <jsp:param name="" value=""/>
                <jsp:param name="" value=""/>
            </jsp:params>
            
            <jsp:fallback>
                Your browser do not support ABC plugin
            </jsp:fallback>
        </jsp:plugin>
        --%>

        <jsp:scriptlet>
            response.flushBuffer();
        </jsp:scriptlet>

        <%--
        <jsp:include flush="true" page="/books"/>
        <jsp:forward page="/books"/>
        --%>

        <jsp:scriptlet>
            out.println("Welcome to Java Code in JSP :(" + 20 * 3);
        </jsp:scriptlet>
        <br/><br/>

        <jsp:expression>"james gosling".toUpperCase()</jsp:expression>

            <div style='font-size: 36px; color:darkred'>JavaServer Pages</div>

        <jsp:element name="div">
            JavaServer Pages
        </jsp:element>

        <jsp:element name="div">
            <jsp:attribute name="style" trim="true">
                font-size: 36px; color:darkgreen
            </jsp:attribute>
            <jsp:body>
                JavaServer<sup>&copy;</sup> Pages
            </jsp:body>
        </jsp:element>

    </body>
</html>
















