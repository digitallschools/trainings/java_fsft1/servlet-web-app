<%-- 
    Document   : viewBooks
    Created on : 06-Jan-2020, 4:02:15 PM
    Author     : DigitallSchool <rupeshkumar@digitallschool.com>
--%>

<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="/mainNavs"/><br/>


        <c:choose>
            <c:when test="${(not empty requestScope.authorsData) and (requestScope.authorsData.size() eq 0)}">
                <h2>No Authors Exist</h2>
            </c:when>
            <c:otherwise>
                <table border='1'>
                    <tr><th>ID</th><th>Name</th><th>Email</th><th>Ops</th></tr>
                            <c:forEach items="${requestScope.authorsData}" var="author">
                        <tr>
                            <td>${author.authorId}</td>
                            <td>${author.name}</td>
                            <td>${author.email}</td>
                            <td>
                                <c:url var="deleteAuthorURL" value="authors">
                                    <c:param name="aid" value="${author.authorId}"/>
                                </c:url>

                                <a href='<c:out value="${deleteAuthorURL}"/>'>Delete</a>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:otherwise>
        </c:choose>
        <br/><br/>

        <sql:setDataSource var="ds" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost:3306/spider_library"
                           user="root" password="root"/>

        <sql:query dataSource="${ds}" var="authorsResult" sql="SELECT * FROM authors"/>


        <table border='1'>
            <tr><th>ID</th><th>Name</th><th>Email</th><th>Ops</th></tr>
                    <c:forEach items="${authorsResult.rowsByIndex}" var="author">
                <tr>
                    <td>${author[0]}</td>
                    <td>${author[1]}</td>
                    <td>${author[2]}</td>
                    <td>
                        <c:url var="deleteAuthorURL2" value="authors">
                            <c:param name="aid" value="${author[0]}"/>
                        </c:url>

                        <a href='<c:out value="${deleteAuthorURL2}"/>'>Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
