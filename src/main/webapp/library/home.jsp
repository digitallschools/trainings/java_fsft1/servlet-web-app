<%-- 
    Document   : home
    Created on : 02-Jan-2020, 5:30:20 PM
    Author     : DigitallSchool <rupeshkumar@digitallschool.com>
--%>

<%@page import="java.time.LocalDate, java.util.*, java.io.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8" autoFlush="true"
        buffer="20kb" info="Learning JSP Technology" language="java"
        isELIgnored="true" isThreadSafe="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv='Content-Type' content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        Hello: ${param.name}<br/>
        <div style='font-size: 40px'>
            Hello: <%= request.getParameter("name")%>
        </div>
        <%= 5 + 6 + i%><br/>
        <%= LocalDate.now()%>
        <h1>Hello World!</h1>
        <%-- JSP Comment, Learning JavaServer Pages --%>
        <%
            out.println(new java.util.Date());

        %>
    </body>
</html>

<%!
    int i = 256;

    public void doWork() {
//do some job
    }
%>










