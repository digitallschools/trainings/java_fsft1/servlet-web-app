/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web.library;

import com.digitallschool.training.spiders.web.library.model.AuthorService;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@WebServlet(urlPatterns = {"/authors2"})
public class AuthorServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AuthorService service = new AuthorService();
        PrintWriter out = response.getWriter();

        try {
            request.setAttribute("authorsData", service.getAuthors());
            request.getRequestDispatcher("library/viewBooks.jsp").forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace(out);
            return;
        }
    }

}
