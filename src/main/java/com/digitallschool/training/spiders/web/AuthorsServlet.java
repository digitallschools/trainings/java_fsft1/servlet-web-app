/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@WebServlet(urlPatterns = {"/authors"})
public class AuthorsServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<html><head><title>Authors Home</title></head><body>");

        request.getRequestDispatcher("/mainNavs").include(request, response);

        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                    Statement st = con.createStatement();
                    ResultSet rs = st.executeQuery("SELECT * FROM authors");) {

                if (rs.next()) {
                    out.println("<table style='float:left' border='1'>"
                            + "<caption>Authors Details</caption>"
                            + "<tr><th>Author ID</th><th>Name</th><th>Email</th></tr>");
                    do {
                        out.println("<tr>"
                                + "<td>" + rs.getInt(1) + "</td>"
                                + "<td>" + rs.getString(2) + "</td>"
                                + "<td>" + rs.getString(3) + "</td>"
                                + "</tr>");
                    } while (rs.next());
                    out.println("</table>");
                } else {
                    out.println("<h2>No Authors available.</h2>");
                }
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace(out);
        }

        out.println("<form method='post' action='authors' style='float:right'>"
                + "<table><tr><th colspan='2'>New Author Details</th></tr>"
                + "<tr><td>Name</td><td><input type='text' name='name' size='35'/></td></tr>"
                + "<tr><td>Email</td><td><input type='text' name='mail' size='50'/></td></tr>"
                + "<tr><td>Profile</td><td><textarea name='profile' rows='5' cols='50'></textarea></td></tr>"
                + "<tr><td>Blog</td><td><input type='text' name='blog' size='50'/></td></tr>"
                + "<tr><td colspan='2' align='right'><input type='submit' value='Add Author'/></td></tr>"
                + "</table>");

        out.println("</body></html>");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                    PreparedStatement pst = con.prepareStatement("INSERT INTO authors VALUES(0, ?, ?, ?, ?)")) {
                String authorName = request.getParameter("name");
                String email = request.getParameter("mail");
                String profile = request.getParameter("profile");
                String blog = request.getParameter("blog");

                pst.setString(1, authorName);
                pst.setString(2, email);
                pst.setString(3, profile);
                pst.setString(4, blog);

                pst.executeUpdate();

                response.sendRedirect("authors");
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace(out);
        }
    }
}
