/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web.library.model;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
public class AuthorService {

    private static String jdbc_driver = "com.mysql.jdbc.Driver";
    private String jdbc_url = "jdbc:mysql://localhost:3306/spider_library";
    private String username = "root";
    private String pwd = "root";

    static {
        try {
            Class.forName(jdbc_driver);
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
    }

    public List<Author> getAuthors() throws SQLException {
        List<Author> authors = new ArrayList<>();

        try (Connection con = DriverManager.getConnection(jdbc_url, username, pwd);
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM authors")) {

            while (rs.next()) {
                authors.add(new Author(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5)));
            }

        } catch (SQLException sqle) {
            throw sqle;
        }

        return authors;
    }
}
