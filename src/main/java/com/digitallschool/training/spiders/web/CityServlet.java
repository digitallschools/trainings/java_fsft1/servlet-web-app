/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@WebServlet(urlPatterns = {"/cities"})
public class CityServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");
                    Statement st = con.createStatement();
                    ResultSet rs = st.executeQuery("SELECT * FROM city");) {

                if (rs.next()) {
                    out.println("<table border='1'><tr><th>City ID</th><th>Name</th><th>Country ID</th></tr>");
                    do {
                        out.println("<tr>"
                                + "<td>" + rs.getInt(1) + "</td>"
                                + "<td>" + rs.getString(2) + "</td>"
                                + "<td>" + rs.getInt(3) + "</td>"
                                + "</tr>");
                    } while (rs.next());
                    out.println("</table>");
                } else {
                    out.println("<h2>Cities EMPTY</h2>");
                }
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {

        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");
                    PreparedStatement pst = con.prepareStatement("INSERT INTO city (city, country_id) VALUES(?, ?)")) {
                    int countryId = Integer.parseInt(request.getParameter("cid"));
                    String cityName = request.getParameter("name");
                    
                    pst.setInt(2, countryId);
                    pst.setString(1, cityName);
                    
                    pst.executeUpdate();
                    
                    response.sendRedirect("cities");
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {

        }
    }
}
