/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web.library;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
//@WebServlet(urlPatterns = {"/books"})
public class BooksServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<html><head><title>Authors Home</title></head><body>");
        
        request.getRequestDispatcher("/mainNavs").include(request, response);

        out.println("<div style='width: 100%;float: left'>");

        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                    Statement st = con.createStatement();
                    ResultSet rs = st.executeQuery("SELECT * FROM books");) {

                //out.println("<h1>" + response.getBufferSize() + "</h1>");
                if (rs.next()) {
                    out.println("<table style='float:left' border='1'>"
                            + "<caption>Books Details</caption>"
                            + "<tr><th>Book ID</th><th>Title</th><th>Edition</th><th>Price</th>"
                            + "<th>Year</th><th>Oper<sup>s</sup></th></tr>");
                    do {
                        out.println("<tr>"
                                + "<td>" + rs.getInt(1) + "</td>"
                                + "<td>" + rs.getString(2) + "</td>"
                                + "<td>" + rs.getInt(3) + "</td>"
                                + "<td>" + rs.getDouble(4) + "</td>"
                                + "<td>" + Year.parse(rs.getDate(5).toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd")) + "</td>"
                                + "<td><form style='display:inline' method='post' action='deleteBook'>"
                                        + "<input type='hidden' name='bid' value='" + rs.getInt(1) + "'/>"
                                                + "<input type='submit' value='Delete'/></td>"
                                + "</tr>");
                    } while (rs.next());
                    out.println("</table>");
                } else {
                    out.println("<h2>No Books available.</h2>");
                }
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace(out);
        }

        out.println("<form method='post' action='books' style='float:right'>"
                + "<table><tr><th colspan='2'>New Book Details</th></tr>"
                + "<tr><td>Title</td><td><input type='text' name='title' size='35'/></td></tr>"
                + "<tr><td>Edition</td><td><input type='text' name='edition' size='2'/></td></tr>"
                + "<tr><td>Price</td><td><input type='text' name='price' size='4'/></td></tr>"
                + "<tr><td>Year of Publishing</td><td><input type='text' name='year' size='3'/></td></tr>");

        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT publisher_id, name FROM publishers");) {

            if (rs.next()) {
                out.println("<tr><td>Publishers</td><td><select name='pid'><option value=''>-- Select Publisher --</option>");
                do {
                    int publisherId = rs.getInt(1);
                    String name = rs.getString(2);
                    out.println("<option value='" + publisherId + "'>" + name + "</option>");
                } while (rs.next());
                out.println("</select></td></tr>");
            }
        } catch (Exception e) {
            e.printStackTrace(out);
        }

        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT author_id, name FROM authors");) {

            if (rs.next()) {
                out.println("<tr><td>Authors</td><td><select name='aid' multiple size='5'><option value=''>-- Select Authors --</option>");
                do {
                    int authorId = rs.getInt(1);
                    String name = rs.getString(2);
                    out.println("<option value='" + authorId + "'>" + name + "</option>");
                } while (rs.next());
                out.println("</select></td></tr>");
            }
        } catch (Exception e) {
            e.printStackTrace(out);
        }

        out.println("<tr><td colspan='2' align='right'><input type='submit' value='Add Book'/></td></tr>"
                + "</table></form></div>");

        out.println("</body></html>");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                    PreparedStatement pst = con.prepareStatement("INSERT INTO books VALUES(0, ?, ?, ?, ?, ?)");
                    PreparedStatement pst2 = con.prepareStatement("INSERT INTO book_authors VALUES(0, ?, ?, ?)")) {
                con.setAutoCommit(false);

                pst.setString(1, request.getParameter("title"));
                pst.setInt(2, Integer.parseInt(request.getParameter("edition")));
                pst.setDouble(3, Double.parseDouble(request.getParameter("price")));
                //pst.setDate(4, Date.valueOf(request.getParameter("year") + "-01-01"));
                pst.setString(4, request.getParameter("year"));
                pst.setInt(5, Integer.parseInt(request.getParameter("pid")));

                pst.executeUpdate();

                int bookId = 0;
                PreparedStatement pst3
                        = con.prepareStatement("SELECT book_id FROM books WHERE title=? AND edition=? AND price=? AND publisher=?");
                pst3.setString(1, request.getParameter("title"));
                pst3.setInt(2, Integer.parseInt(request.getParameter("edition")));
                pst3.setDouble(3, Double.parseDouble(request.getParameter("price")));
                pst3.setInt(4, Integer.parseInt(request.getParameter("pid")));

                ResultSet rs = pst3.executeQuery();
                if (rs.next()) {
                    bookId = rs.getInt(1);
                }

                String[] authorIdList = request.getParameterValues("aid");
                pst2.setInt(1, bookId);
                pst2.setString(3, "");

                for (String id : authorIdList) {
                    pst2.setInt(2, Integer.parseInt(id));
                    pst2.executeUpdate();
                }

                con.commit();
                con.setAutoCommit(true);
                response.sendRedirect("books");
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace(out);
        }
    }
}
