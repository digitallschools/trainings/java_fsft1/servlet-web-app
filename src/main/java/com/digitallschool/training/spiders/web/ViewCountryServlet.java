/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@WebServlet(urlPatterns = {"/viewCountries"})
public class ViewCountryServlet extends GenericServlet {

    @Override
    public void service(ServletRequest req, ServletResponse res)
            throws ServletException, IOException {
        res.setContentType("text/html");
        PrintWriter out = res.getWriter();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "root");
                    Statement st = con.createStatement();
                    ResultSet rs = st.executeQuery("SELECT * FROM country");) {

                if (rs.next()) {
                    out.println("<table border='1'><tr><th>Country ID</th><th>Name</th></tr>");
                    do {
                        out.println("<tr><td>" + rs.getInt(1) + "</td><td>" + rs.getString(2) + "</td></tr>");
                    } while (rs.next());
                    out.println("</table>");
                } else {
                    out.println("<h2>Countries EMPTY</h2>");
                }
            } catch (Exception e) {
                e.printStackTrace(out);
            }
        } catch (ClassNotFoundException cnfe) {

        }
    }

}
