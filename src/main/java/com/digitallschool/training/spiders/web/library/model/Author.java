/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web.library.model;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbPropertyOrder;
import javax.json.bind.config.PropertyOrderStrategy;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@JsonbPropertyOrder(PropertyOrderStrategy.REVERSE)
public class Author {

    @JsonbProperty("author-id")
    private int authorId;
    private String name;
    private String email;
    private String profile;
    @JsonbProperty("blog-address")
    private String blog;

    public Author() {
        super();
    }

    public Author(int authorId, String name, String email, String profile, String blog) {
        this.authorId = authorId;
        this.name = name;
        this.email = email;
        this.profile = profile;
        this.blog = blog;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

}
