/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web.library;

import com.digitallschool.training.spiders.web.library.model.Author;
import com.digitallschool.training.spiders.web.library.model.AuthorService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyOrderStrategy;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@WebServlet(urlPatterns = {"/authorsJSON"})
public class AuthorJSONServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AuthorService authorService = new AuthorService();
        PrintWriter out = response.getWriter();
        
        response.setContentType("application/json");
        
        try {
            List<Author> authors = authorService.getAuthors();
            
            Jsonb jsonb = JsonbBuilder.create(
                    new JsonbConfig()
                            .withPropertyOrderStrategy(PropertyOrderStrategy.REVERSE)
            );

            /*String result = jsonb.toJson(authors);
            out.println(result);*/
            jsonb.toJson(authors, out);
            
        } catch (Exception e) {
            e.printStackTrace(out);
        }
    }
    
}
