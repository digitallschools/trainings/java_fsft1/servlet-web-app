/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web.library;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@WebServlet(urlPatterns = {"/deleteBook"})
public class DeleteBookServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        int bookId = Integer.parseInt(request.getParameter("bid"));

        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/spider_library", "root", "root");
                PreparedStatement pst1 = con.prepareStatement("DELETE FROM books WHERE book_id=?");
                PreparedStatement pst2 = con.prepareStatement("DELETE FROM book_authors WHERE book_id=?");) {
            con.setAutoCommit(false);
            
            pst2.setInt(1, bookId);
            pst2.executeUpdate();
            
            pst1.setInt(1, bookId);
            pst1.executeUpdate();

            con.commit();
            
            response.sendRedirect("books");
        } catch (Exception e) {
            e.printStackTrace(out);
        }
    }
    
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
