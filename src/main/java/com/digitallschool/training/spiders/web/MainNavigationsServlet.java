/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digitallschool.training.spiders.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author DigitallSchool <rupeshkumar@digitallschool.com>
 */
@WebServlet(urlPatterns = {"/mainNavs"})
public class MainNavigationsServlet extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        out.println("<div><a href='publishers'>Publishers</a> "
                + "<a href='authors2'>Authors</a> "
                + "<a href='books'>Books</a> "
                + "<br/><hr color='darkgreen'/><br/><br/></div>");
    }
}
